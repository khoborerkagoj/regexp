#!/usr/bin/python

import sys
if len(sys.argv) < 2:
    raise ValueError("No regexp specified")

parsedRegexp = [];
regexp = sys.argv[1];
