#include <iostream>
#include <cstring>

namespace Regexp {
enum RegexpType { STAR, ALT, CHAR, GROUP };
class Regexp {
  Type type;
  Regexp *next;
  Regexp *val[2];
  char match;

public:
  Regexp(Type t, char match);
  Regexp(Type t, const Regexp *val1, const Regexp *val2 = NULL);
};
}

namespace Regexp {
class Buf {
  int idx;
  int N;
  const char *str;
  char ungetcVal = 0;
public:
  Buf(const char *);
  char getc();
  void ungetc(char c) { ungetcVal = c; }
  enum bufRet { STAR = -1, ALT = -2, OPEN = -3, CLOSE = -4, ERR = -127};
};
}

Regexp::Buf::Buf(const char *c) {
  str = c;
  idx = 0;
  ungetcVal = 0;
}

char Regexp::Buf::getc() {
  char c;
  const char specials[] = "btnr";
  const char specVals[] = "\b\t\n\r";
  const char *match;

  if (ungetcVal) {
    c = ungetcVal;
    ungetcVal = 0;
    return c;
  }
  if (str[idx] == '\0')
    return 0;
  if ((c = str[idx++]) == '\\') {
    if ((c = str[idx++]) == '\0') {
      std::cerr << "Trailing backslash" << endl;
      return (char)ERR;
    }
    if (match = strstr(c, specials))
      return *(specVals + (c - specials));
    else
      return c;
  } else {
    switch (c) {
      case '*': return (char)STAR;  break;
      case '|': return (char)ALT;   break;
      case '(': return (char)OPEN;  break;
      case ')': return (char)CLOSE; break;
      default:  return c;           break;
    }
  }
}

Regexp::Regexp::Regexp(Type t, char match) {
  if (t != CHAR) {
    cerr << "Incorrect initializer called with type " << t << endl;
  }
  this->type  = t;
  this->match = match;
}

Regexp::Regexp::Regexp(Type t, const Regexp *val1, const Regexp *val2) {
  if (this->type == CHAR) {
    cerr << "Unexpected initializer of type CHAR" << endl;
  }
  this->type = t;
  this->val1 = val1;
  this->val2 = val2;
}